package ru.t1.semikolenov.tm.exception.field;

public final class IncorrectDateBeginException extends AbstractFieldException {

    public IncorrectDateBeginException() {
        super("Error! Data begin is incorrect...");
    }

}
