package ru.t1.semikolenov.tm.exception.user;

import ru.t1.semikolenov.tm.exception.field.AbstractFieldException;

public final class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}
