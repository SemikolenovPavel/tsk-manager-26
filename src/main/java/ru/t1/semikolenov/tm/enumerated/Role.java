package ru.t1.semikolenov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Administrator"),
    USUAL("Usual user");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
