package ru.t1.semikolenov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsShowCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show commands list.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        System.out.println("TASK MANAGER COMMANDS:");
        for (@NotNull final ICommand command : commands) {
            final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
