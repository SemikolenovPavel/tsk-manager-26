package ru.t1.semikolenov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.semikolenov.tm.api.repository.ICommandRepository;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.repository.IUserRepository;
import ru.t1.semikolenov.tm.api.service.*;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.semikolenov.tm.exception.system.CommandNotSupportedException;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.repository.CommandRepository;
import ru.t1.semikolenov.tm.repository.ProjectRepository;
import ru.t1.semikolenov.tm.repository.TaskRepository;
import ru.t1.semikolenov.tm.repository.UserRepository;
import ru.t1.semikolenov.tm.service.*;
import ru.t1.semikolenov.tm.util.DateUtil;
import ru.t1.semikolenov.tm.util.SystemUtil;
import ru.t1.semikolenov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.semikolenov.tm.command";

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final User user = userService.findByLogin("test");
        if (user == null) return;
        @NotNull final String userId = user.getId();
        taskService.add(userId, new Task("DEMO TASK-12", Status.NOT_STARTED, DateUtil.toDate("01.01.2020")));
        taskService.add(userId, new Task("DEMO TASK-14", Status.COMPLETED, DateUtil.toDate("02.02.2020")));
        taskService.add(userId, new Task("DEMO TASK-11", Status.IN_PROGRESS, DateUtil.toDate("03.03.2020")));
        taskService.add(userId, new Task("DEMO TASK-13", Status.IN_PROGRESS, DateUtil.toDate("04.04.2020")));
        projectService.add(userId, new Project("DEMO PROJECT-12", Status.NOT_STARTED, DateUtil.toDate("01.01.2020")));
        projectService.add(userId, new Project("DEMO PROJECT-14", Status.COMPLETED, DateUtil.toDate("02.02.2020")));
        projectService.add(userId, new Project("DEMO PROJECT-11", Status.IN_PROGRESS, DateUtil.toDate("03.03.2020")));
        projectService.add(userId, new Project("DEMO PROJECT-13", Status.IN_PROGRESS, DateUtil.toDate("04.04.2020")));
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
            reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final Object object = clazz.newInstance();
        final AbstractCommand command = (AbstractCommand) object;
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void run(@Nullable final String[] args) {
        try {
            if (processArgument(args))
                System.exit(0);
            initPID();
            initData();
            initLogger();
        }
        catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            }
            catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

}
